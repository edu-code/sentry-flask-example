import os
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, render_template
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS

db = SQLAlchemy()
migrate = Migrate()


def create_app(config_class=Config):
    app = Flask(__name__, static_folder='../static/static', template_folder='../static')
    app.config.from_object(config_class)

    # enable CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    db.init_app(app)
    migrate.init_app(app, db)

    @app.route('/', defaults={'path':''})
    @app.route('/<path:path>')
    def catch_all(path):
        return render_template("index.html")

    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    if not app.debug and not app.testing:
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/car-api.log', maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d'))
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('CAR-API startup')

    return app
