<template>
  <div class="container">
    <div class="row">
      <div class="col-sm-10">
        <h1>Cars</h1>
        <hr />
        <br />
        <br />
        <button type="button" class="btn btn-success btn-sm" v-b-modal.car-modal>Add Car</button>
        <br />
        <br />
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">Make</th>
              <th scope="col">Model</th>
              <th scope="col">Year</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(car, index) in cars" :key="index">
              <td>{{ car.car_make }}</td>
              <td>{{ car.car_model }}</td>
              <td>{{ car.car_year }}</td>
              <td>
                <button type="button"
                        class="btn btn-warning btn-sm"
                        @click=OnUpdateCar(car)>Update</button>
                <button
                        type="button"
                        class="btn btn-danger btn-sm"
                        @click=onDeleteCar(car)>Delete</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <b-modal ref="addCarModal" id="car-modal" title="Add a new car" hide-footer>
      <b-form @submit="onSubmit" @reset="onReset" class="w-100">
        <b-form-group id="form-title-group" label="Make:" label-for="form-title-input">
          <b-form-input
            id="form-make-input"
            type="text"
            v-model="addCarForm.car_make"
            required
            placeholder="Enter make"
          ></b-form-input>
        </b-form-group>
        <b-form-group id="form-author-group" label="Model:" label-for="form-author-input">
          <b-form-input
            id="form-model-input"
            type="text"
            v-model="addCarForm.car_model"
            required
            placeholder="Enter model"
          ></b-form-input>
        </b-form-group>
        <b-form-group id="form-author-group" label="Year:" label-for="form-author-input">
          <b-form-input
            id="form-year-input"
            type="text"
            v-model="addCarForm.car_year"
            required
            placeholder="Enter year"
          ></b-form-input>
        </b-form-group>
        <b-button type="submit" variant="primary">Submit</b-button>
        <b-button type="reset" variant="danger">Reset</b-button>
      </b-form>
    </b-modal>
  </div>
</template>

<script>
import axios from 'axios';

export default {
  data() {
    return {
      cars: [],
      addCarForm: {
        car_make: '',
        car_model: '',
        car_year: '',
      },
    };
  },
  methods: {
    getCars() {
      const path = 'http://134.209.207.213:80/api/cars';
      axios
        .get(path)
        .then((res) => {
          this.cars = res.data.items;
        })
        .catch((error) => {
          // eslint-disable-next-line
          Sentry.captureException(error);
        });
    },
    addCar(payload) {
      const path = 'http://134.209.207.213:80/api/cars';
      axios
        .post(path, payload)
        .then(() => {
          this.getCars();
        })
        .catch((error) => {
          // eslint-disable-next-line
          Sentry.captureException(error);
          this.getCars();
        });
    },
    initForm() {
      this.addCarForm.car_make = '';
      this.addCarForm.car_model = '';
      this.addCarForm.car_year = '';
    },
    onSubmit(e) {
      e.preventDefault();
      this.$refs.addCarModal.hide();
      const payload = {
        car_make: this.addCarForm.car_make,
        car_model: this.addCarForm.car_model,
        car_year: this.addCarForm.car_year,
      };
      this.addCar(payload);
      this.initForm();
    },
    onReset(e) {
      e.preventDefault();
      this.$refs.addCarModal.hide();
      this.initForm();
    },
    removeCar(carID) {
      const path = `http://134.209.207.213:80/api/cars/${carID}`;
      axios.delete(path)
        .then(() => {
          this.getCars();
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
          this.getCars();
        });
    },
    onDeleteCar(car) {
      this.removeCar(car.id);
    },
    onUpdateCar(car) {
      this.updateCar(car);
    },
  },
  created() {
    this.getCars();
  },
};
</script>
